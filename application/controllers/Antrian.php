<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Antrian extends CI_Controller 
{
	public function __construct(){
		parent::__construct();
		
		$this->load->model("model_antrian");
		$this->load->helper('url');
	}
	public function panggil(){
		$id=$this->input->post('id');
		$data = array(
			'status'=> '1',
		);
		$this->model_antrian->update(array('antrian_id' => $id), $data);
		echo json_encode(array("status" => TRUE));
	}
	public function get_antrian_now(){
		$id=$this->input->get('id');
		$data =$this->model_antrian->get_antrian_now($id);
		echo json_encode($data);
	} 

	public function get_antrian_counter(){
		$id=$this->input->get('id');
		$data =$this->model_antrian->get_antrian_counter($id);
		echo json_encode($data);
	} 

	public function get_antrian_nexts(){
		$id=$this->input->get('id');
		$data =$this->model_antrian->get_antrian_next($id);
		echo json_encode($data);
	} 

	
}