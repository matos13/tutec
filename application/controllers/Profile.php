<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("model_user");
		$this->load->helper('url');
	}

	public function index()
	{	
		$id=($this->session->userdata['logged_in']['user_id']);
		$data['user_info']=$this->model_user->get_by_id($id);
		$this->load->view("masterdata/profile/index",$data);
		// $this->load->view("masterdata/profile/index");
	}

	public function ajax_update()
	{
		$user_id=$this->input->post("user_id");
		$user_username=$this->input->post("user_username");
		$user_info=$this->input->post("user_info");

		$user_password=$this->input->post("user_password");
		$user_password_2=$this->input->post("user_password_2");

		if ($user_password_2 != $user_password) {
			# code...
			echo json_encode(array("status" => FALSE));
		} else if ($user_password == null) {
			# code...
			$data = array(
				'user_username'=> $user_username,
				'user_info'=> $user_info
				
			);
		} else {
			$data = array(
				'user_username'=> $user_username,
				'user_info'=> $user_info,
				'user_password'=> $user_password
				
			);
		}

		
		
		$this->model_user->update(array('user_id' => $user_id), $data);
		echo json_encode(array("status" => TRUE));
	}

	
}
