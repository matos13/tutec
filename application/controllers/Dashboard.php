<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller 
{
	public function __construct(){
		parent::__construct();
		$this->load->model("model_antrian");
		$this->load->helper('url');
	}
	public function waktu(){
		$bulan = array("Januari","Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
		$hari = array("Senin","Selasa","Rabu","Kamis","Jumat","Sabtu","Minggu");

		$time = $hari[date("N")-1].", ".date("d")." ".$bulan[date("m")-1]." ".date("Y");

		echo json_encode(array("waktu"=>$time));

	}
	public function antrian(){
		$now =date("Y-m-d H:i:s");
		$status =0;
		$nomor =$this->input->post('nomor_atrian');
		$poli =$this->input->post('poli_atrian');

		$data=array(
			'nomor'=>$nomor,
			'waktu'=>$now,
			'status'=>$status,
			'poli_id'=>$poli,
		);
		$insert = $this->model_antrian->add_antrian($data);
		echo json_encode(array("status" => TRUE));
	}
	
}