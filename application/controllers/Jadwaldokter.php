<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwaldokter extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("model_jadwaldokter");
		$this->load->helper('url');
	}
	public function ajax_list()
	{
		$list = $this->model_jadwaldokter->get_datatables();
		$data = array();
		$no = $_POST['start'];
		$sts;
		foreach ($list as $jadwaldokter) {
			$no++;

			$hariini = $jadwaldokter->jd_hari_mulai;

			if ($hariini == 1) {
				$hariini = "Senin";
			} else if ($hariini == 2) {
				$hariini = "Selasa";
			} else if ($hariini == 3) {
				$hariini = "Rabu";
			} else if ($hariini == 4) {
				$hariini = "Kamis";
			} else if ($hariini == 5) {
				$hariini = "Jumat";
			} else if ($hariini == 6) {
				$hariini = "Sabtu";
			} else {
				$hariini = "Minggu";
			}

			$jadwal_praktek = $hariini."&nbsp".date("H.i",strtotime($jadwaldokter->jd_jam_mulai))." - ".date("H.i",strtotime($jadwaldokter->jd_jam_selesai));
			

			if ($jadwaldokter->jd_kehadiran==1) {
				$sts='<span class="btn btn-success">Hadir</span>';
			}else{
				$sts='<span class="btn btn-danger">Tidak Hadir</span>';
			}
			$row = array();;
			$row[] = $no;
			$row[] = $jadwaldokter->dokter_nama;
			$row[] = "Poli ".$jadwaldokter->nama;
			$row[] = $jadwal_praktek;
			$row[] = $sts;
			// $row[] = $sts;
			$row[] = '<button data-toggle="tooltip" data-placement="top" title="Edit" type="button" class="btn btn-info btn-outline btn-circle btn-lg m-r-5"> <a href="'.base_url().'masterdata/jadwaldokter_edit?id='.$jadwaldokter->jd_id.'"><i class="ti-pencil-alt"></i></a></button>
			<button data-toggle="tooltip" data-placement="top" id="sa-Delete" title="Hapus" type="button" class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i class="icon-trash" onclick="delete_jadwaldokter('."'".$jadwaldokter->jd_id."'".')"></i></button>
			';
			$data[] = $row;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->model_jadwaldokter->count_all(),
			"recordsFiltered" => $this->model_jadwaldokter->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}
	public function ajax_delete($id)
	{
		$this->model_jadwaldokter->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}
	public function ajax_edit($id)
	{
		$data=$this->model_jadwaldokter->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add()
	{
        // print_r($this->input->post());
        // exit();
		$jd_id=$this->input->post("jd_id");
		$jd_dokter=$this->input->post("jd_dokter");
		$jd_poli=$this->input->post("jd_poli");
		$jd_hari_mulai=$this->input->post("jd_hari_mulai");
		$jd_hari_selesai=$this->input->post("jd_hari_selesai");
		$jd_jam_mulai=$this->input->post("jd_jam_mulai");
		$jd_jam_selesai=$this->input->post("jd_jam_selesai");
		$jd_kehadiran=$this->input->post("jd_kehadiran");
		$data = array(
			'jd_dokter'=> $jd_dokter,
			'jd_poli'=> $jd_poli,
			'jd_hari_mulai'=> $jd_hari_mulai,
			'jd_jam_mulai'=> $jd_jam_mulai,
			'jd_jam_selesai'=> $jd_jam_selesai,
			'jd_kehadiran'=> $jd_kehadiran
		);
		$insert = $this->model_jadwaldokter->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$jd_id=$this->input->post("jd_id");
		$jd_dokter=$this->input->post("jd_dokter");
		$jd_poli=$this->input->post("jd_poli");
		$jd_hari_mulai=$this->input->post("jd_hari_mulai");
		// $jd_hari_selesai=$this->input->post("jd_hari_selesai");
		$jd_jam_mulai=$this->input->post("jd_jam_mulai");
		$jd_jam_selesai=$this->input->post("jd_jam_selesai");
		$jd_kehadiran=$this->input->post("jd_kehadiran");
		$data = array(
			'jd_dokter'=> $jd_dokter,
			'jd_poli'=> $jd_poli,
			'jd_hari_mulai'=> $jd_hari_mulai,
			'jd_jam_mulai'=> $jd_jam_mulai,
			'jd_jam_selesai'=> $jd_jam_selesai,
			'jd_kehadiran'=> $jd_kehadiran
		);
		$this->model_jadwaldokter->update(array('jd_id' => $jd_id), $data);
		echo json_encode(array("status" => TRUE));
	}



}
