<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Masterdata extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("model_dokter");
		$this->load->model("model_poli");
		$this->load->model("model_antrian");
		$this->load->model("model_jadwaldokter");
		$this->load->helper('url');
	}

	// user controller here
	public function user_page()
	{
		$this->load->view("masterdata/user/index");
	}
	public function user_add()
	{
		$this->load->view("masterdata/user/add");
	}

	// user controller here
	public function jadwaldokter_page()
	{
		$data['dokter']=$this->model_dokter->get_data();
		$data['poli']=$this->model_poli->get_data();
		$this->load->view("masterdata/jadwaldokter/index",$data);
		// $this->load->view("masterdata/jadwaldokter/index");
	}
	public function jadwaldokter_edit()
	{
		$id=$this->input->get('id');
		$data['dokter']=$this->model_dokter->get_data();
		$data['poli']=$this->model_poli->get_data();
		$data['edit']=$this->model_jadwaldokter->get_by_id($id);
		$this->load->view("masterdata/jadwaldokter/jadwaldokter-edit",$data);
		// $this->load->view("masterdata/jadwaldokter/index");
	}
	public function counter()
	{
		$data['poli']=$this->model_antrian->get_antrian();
		// $data['poli']['max']=$this->model_antrian->get_last_atrian();
		$this->load->view("counter/index",$data);

	}
	public function jadwaldokter_add()
	{
		$data['dokter']=$this->model_dokter->get_data();
		$data['poli']=$this->model_poli->get_data();
		$this->load->view("masterdata/jadwaldokter/add",$data);
	}

	// user controller here
	public function poli_page()
	{
		$this->load->view("masterdata/poli/index");
	}
	public function poli_add()
	{
		$this->load->view("masterdata/poli/add");
	}

	// user controller here
	public function dokter_page()
	{
		$this->load->view("masterdata/dokter/index");
	}
	public function dokter_add()
	{
		$this->load->view("masterdata/dokter/add");
	}
	public function dokter_delete()
	{
		$this->load->view("masterdata/dokter/add");
	}

}
