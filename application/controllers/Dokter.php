<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dokter extends CI_Controller {
public function __construct(){
		parent::__construct();
		$this->load->model("model_dokter");
		$this->load->helper('url');
	}

	public function index()
	{
		$this->load->view("template", $data);
	}

	public function ajax_list()
	{
			$list = $this->model_dokter->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $dokter) {
				$no++;
				$row = array();;
				$row[] = $no;
				$row[] = $dokter->dokter_nama;
				$row[] = $dokter->dokter_alamat;
				$row[] = $dokter->dokter_no_identitas;
				$row[] = $dokter->dokter_kelamin;
				$row[] = $dokter->dokter_sip;
				$row[] = '<button data-toggle="tooltip" data-placement="top" title="Edit" type="button" class="btn btn-info btn-outline btn-circle btn-lg m-r-5" onclick="edit_dokter('."'".$dokter->dokter_id."'".')"><i class="ti-pencil-alt"></i></button>
				<button data-toggle="tooltip" data-placement="top" id="sa-Delete" title="Hapus" type="button" class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i class="icon-trash" onclick="delete_dokter('."'".$dokter->dokter_id."'".')"></i></button>
					  ';
				$data[] = $row;
			}
			$output = array(
							"draw" => $_POST['draw'],
							"recordsTotal" => $this->model_dokter->count_all(),
							"recordsFiltered" => $this->model_dokter->count_filtered(),
							"data" => $data,
					);
			echo json_encode($output);
		
	}
	public function ajax_delete($id)
	{
		$this->model_dokter->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}
	public function ajax_edit($id)
	{
		$data=$this->model_dokter->get_by_id($id);
			echo json_encode($data);
	}

	public function ajax_add()
	{
        // print_r($this->input->post());
        // exit();
		$dokter_id=$this->input->post("dokter_id");
		$dokter_nama=$this->input->post("dokter_nama");
		$dokter_alamat=$this->input->post("dokter_alamat");
		$dokter_no_identitas=$this->input->post("dokter_no_identitas");
		$dokter_kelamin=$this->input->post("dokter_kelamin");
		$dokter_sip=$this->input->post("dokter_sip");
		$data = array(
			'dokter_nama'=> $dokter_nama,
			'dokter_alamat'=> $dokter_alamat,
			'dokter_no_identitas'=> $dokter_no_identitas,
			'dokter_kelamin'=> $dokter_kelamin,
			'dokter_sip'=> $dokter_sip
		);
		;
		$insert = $this->model_dokter->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$dokter_id=$this->input->post("dokter_id");
		$dokter_nama=$this->input->post("dokter_nama");
		$dokter_alamat=$this->input->post("dokter_alamat");
		$dokter_no_identitas=$this->input->post("dokter_no_identitas");
		$dokter_kelamin=$this->input->post("dokter_kelamin");
		$dokter_sip=$this->input->post("dokter_sip");
		$id=$this->input->post("id_dokter");
		// print_r($this->input->post());
		$data = array(
			'dokter_nama'=> $dokter_nama,
			'dokter_alamat'=> $dokter_alamat,
			'dokter_no_identitas'=> $dokter_no_identitas,
			'dokter_kelamin'=> $dokter_kelamin,
			'dokter_sip'=> $dokter_sip
		);

		// print_r($data);
		$this->model_dokter->update(array('dokter_id' => $id), $data);
		echo json_encode(array("status" => TRUE));
	}



}
