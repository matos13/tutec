<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Login extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
    }
    function index_post() {
        $password = $this->post('password');
        $username = $this->post('username');
        // print_r($this->post());
        // exit();
        if ($username == null || $password == null) {
            # code...
             $this->response([
                    'status' => FALSE,
                    'message' => 'login gagal'
                ], REST_Controller::HTTP_NOT_FOUND); 
        } else {

        $password =md5($password);
        $this->db->where('password', $password)
                ->where('username',$username);
        $pengguna = $this->db->get('pengguna')->result();
        if ($pengguna) {
                $this->response([
                    'status' => TRUE,
                    'message' => 'User login successful.',
                    'data' => $pengguna
                ], REST_Controller::HTTP_OK);
            }else{
                $this->response([
                    'status' => FALSE,
                    'message' => 'login gagal'
                ], REST_Controller::HTTP_NOT_FOUND); 
         }
        }

    }
}