<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Pengguna extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
    }
   
    function index_get() {
        $id = $this->get('id');
        if ($id == '') {
            $pengguna = $this->db->get('pengguna')->result();
        } else {
        $this->db->where('id', $id);
        $pengguna = $this->db->get('pengguna')->result();
        }
        if ($pengguna) {
                $this->response($pengguna, REST_Controller::HTTP_OK);
            }else{
                $this->response([
                    'status' => FALSE,
                    'message' => 'No users were found'
                ], REST_Controller::HTTP_NOT_FOUND); 
         }
    }

    // insert new data to pengguna
    function index_post() {
        // print_r($this->post());
        $pass=md5($this->post('password'));
        $nama=$this->post('nama');
        $username=$this->post('username');
        $email=$this->post('email');
        $no_telp=$this->post('no_telp');
        $jenis_kelamin=$this->post('jenis_kelamin');
        $data = array(
                    'nama'        => $nama,
                    'username'    => $username,
                    'password'    => $pass,
                    'email'       => $email,
                    'no_telp'     => $no_telp,
                    'jenis_kelamin'     => $jenis_kelamin
                );
        $insert = $this->db->insert('pengguna', $data);
        if ($insert) {
            $this->response($data, REST_Controller::HTTP_CREATED);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    // update data pengguna
    function index_put() {
        $id = $this->put('id');
        $pass=md5($this->put('password'));
        $data = array(
            'nama'        => $this->put('nama'),
            'username'    => $this->put('username'),
            'password'    => $pass,
            'email'       => $this->put('email'),
            'no_telp'     => $this->put('no_telp'),
            'jenis_kelamin'     => $this->put('jenis_kelamin'));
        $this->db->where('id', $id);
        $update = $this->db->update('pengguna', $data);
        if ($update) {
            $this->response([
                'status' => TRUE,
                'message' => 'The user info has been updated successfully.'
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    // delete pengguna
    function index_delete() {
        $id = $this->delete('id');
        $this->db->where('id', $id);
        $delete = $this->db->delete('pengguna');
        if ($delete) {
            $this->response(array('status' => 'success'), 201);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

}