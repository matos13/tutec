<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Poli extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
    }
    function index_get() {
        $id = $this->get('id-poli');
        if ($id == '') {
            $poli = $this->db->get('poli')->result();
        } else {
        $this->db->where('poli_id', $id);
        $poli = $this->db->get('poli')->result();
        }
        if ($poli) {
                $this->response([
                    'status'=>TRUE,
                    'data'=>$poli
                ], REST_Controller::HTTP_OK);
            }else{
                $this->response([
                    'status' => FALSE,
                    'message' => 'No data found'
                ], REST_Controller::HTTP_NOT_FOUND); 
            }
        }
 
}