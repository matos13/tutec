<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Antrian extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
    }
    function index_get() {
        $id = $this->get('id-pengguna');
        $id_poli = $this->get('id-poli');
        $tgl = $this->get('tgl');
        $order = $this->get('order');
        // print_r($this->get());

        
        if ($id == ''&& $tgl == ''&& $id_poli!='') {
            $this->db->where('poli_id',$id_poli);
            $antrian = $this->db->get('antrian')->result();

        } elseif($id != ''&& $tgl == '') {
            $this->db->where('id_pengguna', $id)
                    ->where('poli_id',$id_poli);
            $antrian = $this->db->get('antrian')->result();

        }elseif($tgl != '' && $id == ''&& $order =='') {
            $this->db->where('DATE(antrian.waktu)',$tgl)
                     ->where('poli_id',$id_poli);
            $antrian = $this->db->get('antrian')->result();

        }elseif($tgl != ''&&$id != '' && $order =='') {
            $this->db->where('id_pengguna', $id)
                    ->where('DATE(antrian.waktu)',$tgl)
                    ->where('poli_id',$id_poli);
            $antrian = $this->db->get('antrian')->result();

        }elseif($tgl != ''&& $order ='max') {
            $this->db->select("max(nomor) as nomor, antrian_id,waktu,status, poli_id, id_pengguna");
            $this->db->where('DATE(antrian.waktu)',$tgl)
                    ->where('poli_id',$id_poli);
            $antrian = $this->db->get('antrian')->result();
            
        }else{
            $antrian = $this->db->get('antrian')->result();
        }
        if ($antrian) {
                $this->response($antrian, REST_Controller::HTTP_OK);
            }else{
                $this->response([
                    'status' => FALSE,
                    'message' => 'No antrian were found'
                ], REST_Controller::HTTP_NOT_FOUND); 
         }
    }
     function index_post() {
        $id_poli=$this->post('id_poli');
        // $nomor=$this->post('nomor');
        $id_user=$this->post('id_user');
        $tanggal =$this->post('tanggal');
        $date = strtotime("+1 day");
        $now= date("Y-m-d",$date); 
        $this->db->select('max(nomor) as nomor')
            ->from('antrian')
            ->where('antrian.poli_id',$id_poli)
            ->where('DATE(antrian.waktu)',$tanggal);
            $query=$this->db->get();
        $no_atrian= ($query->result());
        $no_atrian =json_decode(json_encode($no_atrian[0]),true);
        $no_now=($no_atrian['nomor']+1);
        if ($tanggal!=$now) {
            $this->response([
                        'status' => FALSE,
                        'message' => 'Booking Hanya bisa dilakukan 1 hari setelah hari ini'
                    ], REST_Controller::HTTP_NOT_FOUND);
        }else{
            $this->db->select('count(*) as jumlah')
            ->from('antrian')
            ->where('antrian.id_pengguna',$id_user)
            ->where('antrian.poli_id',$id_poli)
            ->where('DATE(antrian.waktu)',$tanggal);
            $query=$this->db->get();
            $cek_atrian= ($query->result());
            $jumlah =json_decode(json_encode($cek_atrian[0]),true);
            if ($jumlah['jumlah']<1) {
            $data=array(
                'nomor'=>$no_now,
                'waktu'=>$tanggal,
                'status'=>'0',
                'poli_id'=>$id_poli,
                'id_pengguna'=>$id_user,
            );
            $insert = $this->db->insert('antrian', $data);
            if ($insert) {
                $this->response($data, REST_Controller::HTTP_CREATED);
            } else {
                $this->response(array('status' => 'fail', 502));
            }
            }else{
                $this->response([
                    'status' => FALSE,
                    'message' => 'Antrian hanya boleh 1 kali setiap poli'
                ], REST_Controller::HTTP_NOT_FOUND); 
            }
        }
    }
}