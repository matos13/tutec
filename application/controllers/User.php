<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
public function __construct(){
		parent::__construct();
		$this->load->model("model_user");
		$this->load->helper('url');
	}

	public function index()
	{
		$this->load->view("template", $data);
	}

	public function ajax_list()
	{
		$list = $this->model_user->get_datatables();
		$data = array();
		$no = $_POST['start'];
		$sts;
		foreach ($list as $user) {
			$no++;
			$row = array();;
			$row[] = $no;
			$row[] = $user->user_username;
			$row[] = $user->user_info;
			$row[] = '<button data-toggle="tooltip" data-placement="top" title="Edit" type="button" class="btn btn-info btn-outline btn-circle btn-sm m-r-5" onclick="edit_user('."'".$user->user_id."'".')"><i class="icon wb-edit"></i></button>
			<button data-toggle="tooltip" data-placement="top" id="sa-Delete" title="Hapus" type="button" class="btn btn-info btn-outline btn-circle btn-sm m-r-5"><i class="icon wb-trash" onclick="delete_user('."'".$user->user_id."'".')"></i></button>
				  ';
			$data[] = $row;
		}
		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->model_user->count_all(),
						"recordsFiltered" => $this->model_user->count_filtered(),
						"data" => $data,
				);
		echo json_encode($output);
	}
	public function ajax_delete($id)
	{
		$this->model_user->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}
	public function ajax_edit($id)
	{
		$data=$this->model_user->get_by_id($id);
			echo json_encode($data);
	}

	public function ajax_add()
	{
        // print_r($this->input->post());
        // exit();
		$user_id=$this->input->post("user_id");
		$user_usernama=$this->input->post("user_username");
		$user_password=$this->input->post("user_password");
		$user_info=$this->input->post("user_info");
		$data = array(
			'user_username'=> $user_usernama,
			'user_password'=> $user_password,
			'user_info'=> $user_info,
		);
		;
		$insert = $this->model_user->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$user_username=$this->input->post("user_username");
		$user_info=$this->input->post("user_info");
		$id=$this->input->post("user_id");

		$data = array(
			'user_username' => $user_username, 
			'user_info'=> $user_info,
		);
		$this->model_user->update(array('user_id' => $id), $data);
		echo json_encode(array("status" => TRUE));

	}



}
