<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_home extends CI_Model {

	public function view(){
    return $this->db->get('penilaian')->result();
  }

  public function getpenilaian(){
  $hasil=$this->db->query("SELECT * FROM penilaian join pertanyaan on penilaian.id_pertanyaan=pertanyaan.id_pertanyaan where penilaian.flag_verif='Y'");
  return $hasil->result();
}

 public function getblmpenilaian(){
  $hasil=$this->db->query("SELECT * FROM penilaian join pertanyaan on penilaian.id_pertanyaan=pertanyaan.id_pertanyaan where penilaian.flag_verif='N'");
  return $hasil->result();
}

 public function getpenilaianedit($key){
  $hasil=$this->db->query("SELECT * FROM penilaian join pertanyaan on penilaian.id_pertanyaan=pertanyaan.id_pertanyaan where penilaian.id_penilaian=$key");
  return $hasil->result();
}

 public function getptyjwb($id){
  $hasil=$this->db->query("SELECT COUNT(jawabanpnl) as ptjwb from penilaian where id_user=$id");
  return $hasil->result();
}

public function getpgn(){
  $hasil=$this->db->query("SELECT COUNT(id_user) as pg FROM user WHERE user.jabatan='USR'");
  return $hasil->result();
}

public function getvrf(){
  $hasil=$this->db->query("SELECT COUNT(id_user) as vr FROM user WHERE user.jabatan='VRF'");
  return $hasil->result();
}

public function getpty(){
  $hasil=$this->db->query("SELECT COUNT(id_pertanyaan ) as pt FROM pertanyaan");
  return $hasil->result();
}

public function getfile(){
  $hasil=$this->db->query("SELECT COUNT(nama_file) as fl FROM penilaian");
  return $hasil->result();
}

public function getfileuser($id){
  $hasil=$this->db->query("SELECT COUNT(nama_file) as flus FROM penilaian WHERE id_user=$id");
  return $hasil->result();
}

public function getsdhverif(){
  $hasil=$this->db->query("SELECT COUNT(id_penilaian) as sd FROM penilaian WHERE flag_verif='Y'");
  return $hasil->result();
}

public function getblmverif(){
  $hasil=$this->db->query("SELECT COUNT(id_penilaian) as bl FROM penilaian WHERE flag_verif='N'");
  return $hasil->result();
}

  
  // Fungsi untuk melakukan proses upload file
  public function upload(){
    $config['upload_path'] = './dokumen/';
    $config['allowed_types'] = 'pdf|doc|docx';
    $config['max_size']  = '2048';
    $config['remove_space'] = TRUE;
  
    $this->load->library('upload', $config); // Load konfigurasi uploadnya
    if($this->upload->do_upload('dokumen')){ // Lakukan upload dan Cek jika proses upload berhasil
      // Jika berhasil :
      $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
      return $return;
    }else{
      // Jika gagal :
      $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
      return $return;
    }
  }
  
  // Fungsi untuk menyimpan data ke database
  public function save($upload){
    $x=$this->input->post('nilai');
    if($x==0)
    {
      $v=1;
    }else if($x<=1)
      {
        $v=2;
    }else if($x<=2)
      {
        $v=3;
    }else if($x<=4)
      {
        $v=4;
    }

    if($x==0)
    {
      $r=1;
    }else if($x<=3)
      {
        $r=2;
    }else if($x<=5)
      {
        $r=3;
    }else if($x<=10)
      {
        $r=4;
    }

    if($x==0)
    {
      $i=1;
    }else if($x<=1)
      {
        $i=2;
    }else if($x<=3)
      {
        $i=3;
    }else if($x<=5)
      {
        $i=4;
    }


    if($x==1)
    {
      $o=1;
    }else if($x<=2)
      {
        $o=2;
    }else if($x<=3)
      {
        $o=3;
    }else if($x<=4)
      {
        $o=4;
    }

      $naik = ($x-1)/$x;
      $turun = (4-$x)/$x;
      $hasil = min($v,$r,$i,$o);

      if($v<=1 and $r<=1 and $i<=1 and $o<=1)
      {
        $ket='D';
      }else if($v>1 and $r>1 and $i>1 and $o>1)
      {
        $ket='P';
      }else if($v<=2 and $r<=2 and $i<=2 and $o<=2)
      {
        $ket='P';
      }else if($v>2 and $r>2 and $i>2 and $o>2)
      {
        $ket='T';
      }else if($v<=2 and $r<=2 and $i<=2 and $o<=2)
      {
        $ket='T';
      }else if($v>3 and $r>3 and $i>3 and $o>3)
      {
        $ket='S';
      }



    $data = array(
      'id_penilaian'=>NULL,
      'id_user'=>$this->session->userdata('id_user'),
      'id_pertanyaan'=>$this->input->post('subkategori'),
      'ket_user'=>$this->input->post('ket_user'),
      'jawaban'=>$this->input->post('jawaban'),
      'ket_verif' => '-',
      'nilai'=>$this->input->post('nilai'),
      'nama_file' => $upload['file']['file_name'],
      'flag_verif' => 'N',
      'v' => $v,
      'r' => $r,
      'i' => $i,
      'o' => $o,
      'naik' => $naik,
      'turun' => $turun,
      'hasil' => $hasil,
      'ket_penilaian' => $ket
    );
    
    $this->db->insert('penilaian', $data);
  }
	public function getdata($key)
	{
		//$pwd= md5($p);
		$this->db->where('id_penilaian',$key);
		$hasil = $this->db->get('penilaian');
		return $hasil;

	}

	public function getupdate($key,$data)
	{
		$this->db->where('id_penilaian',$key);
		$this->db->update('penilaian',$data);
	}
	
  public function getdeletepnl($key)
  {
    $this->db->where('id_penilaian',$key);
    $this->db->delete('penilaian');
  }
	
  public function getdatagrafik($key)
  {
    $this->db->where('id_penilaian',$key);
    $this->db->delete('penilaian');
  }

}

/* End of file model_home.php */
/* Location: ./application/models/model_home.php */