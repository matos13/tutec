<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_antrian extends CI_model {

	public function add_antrian($data) {
		$this->db->insert('antrian',$data);
	}

	public function get_antrian(){

		$now =date("Y-m-d");
		$this->db->select('min(antrian.nomor) as nomor ,count(antrian.nomor) as total,max(nomor) as max, poli.poli_id,poli.nama,antrian.antrian_id')
		->from('poli')
		->join('antrian','antrian.poli_id=poli.poli_id','left')
		->where('poli.status',1)
		->where('antrian.status',0)
		->where('DATE(antrian.waktu)',$now)

		->group_by('poli.poli_id');
		$query=$this->db->get();
		return $query->result();
	}
	public function update($where, $data)
	{
		$this->db->update('antrian', $data, $where);
		return $this->db->affected_rows();
	}
	public function get_last_atrian($id)
	{
		$now =date("Y-m-d");
		$this->db->select('max(nomor) as nomor')
		->from('antrian')
		->where('antrian.poli_id',$id)
		->where('DATE(antrian.waktu)',$now);

		$query=$this->db->get();
		return $query->result();
	}
	public function get_min_atrian($id)
	{
		$now =date("Y-m-d");
		$this->db->select('min(nomor) as nomor,antrian_id')
		->from('antrian')
		->where('antrian.poli_id',$id)
		->where('antrian.status',0)
		->where('antrian.waktu',$now)
		->group_by('poli_id,antrian_id');

		$query=$this->db->get();
		return $query->result();
	}
	public function get_antrian_now($id)
	{
		$now =date("Y-m-d");
		$this->db->select('max(nomor) as nomor')
		->from('antrian')
		->where('antrian.poli_id',$id)
		->where('DATE(antrian.waktu)',$now);
		$query=$this->db->get();
		return $query->result();
	}
	public function get_antrian_counter($id)
	{
		$now =date("Y-m-d");
		$this->db->select('min(nomor) as nomor')
		->from('antrian')
		->where('poli_id',$id)
		->where('status',0)
		->where('DATE(antrian.waktu)',$now);

		$query=$this->db->get();
		return $query->result();
	}

	public function get_antrian_next($id)
	{
		$now =date("Y-m-d");
		$this->db->select('min(antrian_id) as minNomor,poli_id')
		->from('antrian')
		->where('poli_id',$id)
		->where('status',0)
		->where('DATE(antrian.waktu)',$now);

		$query=$this->db->get();
		return $query->result();
	}

}