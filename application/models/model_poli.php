<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_poli extends CI_model {
    var $table = 'poli';
	var $column_order = array('nama','status',NULL); //set column field database for datatable orderable
	var $column_search = array('nama','status'); //set column field database for datatable searchable just firstname , lastname , address are searchable
	var $order = array('poli_id' => 'desc');

	public function save($data)
	{
		$this->db->insert('poli',$data);
    }
    public function get_data(){
        $query = $this->db->get('poli');
	    return $query->result();
    }
    public function get_data_poli(){
		// $hariini =date("N");
		// $jamsekarang =date("H:i:s");
		$this->db->select('poli.poli_id,poli.nama')
				->from('poli')
				// ->join('jadwal_dokter','jadwal_dokter.jd_poli=poli.poli_id')
				->where('poli.status',1);
				// ->where('jadwal_dokter.jd_hari_mulai',$hariini)
				// ->where("jadwal_dokter.jd_jam_mulai <='$jamsekarang'")
				// ->where("jadwal_dokter.jd_jam_selesai >='$jamsekarang'")
				// ->where('jadwal_dokter.jd_kehadiran',1);
		$query=$this->db->get();
		return $query->result();
    }
    private function _get_datatables_query()
	{
			$this->db->select('*')
					 ->from('poli');

		$i = 0;
		foreach ($this->column_search as $item) 
		{
			if($_POST['search']['value']) 
			{
				if($i===0) 
				{
					$this->db->group_start(); 
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}
				if(count($this->column_search) - 1 == $i) 
					$this->db->group_end();
			}
			$i++;
		}
		if(isset($_POST['order']))
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
    }
    
    public function delete_by_id($id)
	{
		$this->db->where('poli_id', $id);
		$this->db->delete($this->table);
    }
    public function get_by_id($id)
	{
		$this->db->from($this->table);
		$this->db->where('poli_id',$id);
		$query = $this->db->get();
		return $query->row();
    }
    public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}
}