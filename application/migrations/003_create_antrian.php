<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Migration_create_antrian extends CI_Migration {
public function up() {
        $this->dbforge->add_field(array(
            'antrian_id' => array(
                'type' => 'INT',
                'constraint' => 100,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'nomor' => array(
                'type' => 'INT',
                'constraint' => 100,
            ),
            'waktu' => array(
                'type' => 'DATETIME'
            ),
            'status' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
            ),
            'poli_id' => array(
                'type' => 'INT',
                'constraint' => 100,
            ),
            'id_pengguna' => array(
                'type' => 'INT',
                'constraint' => 100,
            ),
        ));
        $this->dbforge->add_key('antrian_id');
        $this->dbforge->create_table('antrian');
    }
    public function down() {
        $this->dbforge->drop_table('antrian');
    }
}