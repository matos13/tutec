<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Migration_create_keys extends CI_Migration {
public function up() {
        $this->dbforge->add_field(array(
            'key' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
            ),
          
        ));
        $this->dbforge->create_table('keys');
        $data = array(
            array('key' => "tutec123456")
         );
         //$this->db->insert('user_group', $data); I tried both
         $this->db->insert_batch('keys', $data);
    }
    public function down() {
        $this->dbforge->drop_table('keys');
    }
}