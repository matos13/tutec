<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Migration_create_poli extends CI_Migration {
public function up() {
        $this->dbforge->add_field(array(
            'poli_id' => array(
                'type' => 'INT',
                'constraint' => 100,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'nama' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
            ),
            'status' => array(
                'type' => 'VARCHAR',
                'constraint' => '2',
            ),
        ));
        $this->dbforge->add_key('poli_id');
        $this->dbforge->create_table('poli');
        $data = array(
            array('nama' => "Umum")
         );
         $this->db->insert_batch('keys', $data);
        $data = array(
            array('nama' => "Gigi")
         );
         $this->db->insert_batch('keys', $data);
        $data = array(
            array('nama' => "Pemeriksaan Kehamilan")
         );
         $this->db->insert_batch('keys', $data);
        $data = array(
            array('nama' => "Pengambilan Obat")
         );
         $this->db->insert_batch('keys', $data);
    }
    public function down() {
        $this->dbforge->drop_table('poli');
    }
}