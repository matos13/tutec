<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Migration_create_pengguna extends CI_Migration {
public function up() {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 100,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'nama' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
            ),
            'username' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
            ),
            'password' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
            ),
            'email' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
            ),
            'no_telp' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
            ),
            
        ));
        $this->dbforge->add_key('id');
        $this->dbforge->create_table('pengguna');
    }
    public function down() {
        $this->dbforge->drop_table('pengguna');
    }
}