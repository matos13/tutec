    </div>
    <footer class="site-footer">
      <div class="site-footer-legal">© <?php $date = date('Y'); echo $date?> <a href="http://themeforest.net/item/remark-responsive-bootstrap-admin-template/11989202">Tu-Tech</a></div>

    </footer>
    <!-- Core  -->
    <script src="<?php echo base_url(); ?>assets/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/vendor/jquery/jquery.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/vendor/popper-js/umd/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/vendor/bootstrap/bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/vendor/animsition/animsition.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/vendor/mousewheel/jquery.mousewheel.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/vendor/asscrollable/jquery-asScrollable.js"></script>

    <!-- Plugins -->
    <script src="<?php echo base_url(); ?>assets/global/vendor/switchery/switchery.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/vendor/intro-js/intro.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/vendor/screenfull/screenfull.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/vendor/slidepanel/jquery-slidePanel.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/vendor/chartist/chartist.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/vendor/aspieprogress/jquery-asPieProgress.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/vendor/matchheight/jquery.matchHeight-min.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/vendor/jquery-selective/jquery-selective.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>

    <!-- Scripts -->
    <script src="<?php echo base_url(); ?>assets/global/js/Component.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/js/Plugin.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/js/Base.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/js/Config.js"></script>

    <script src="<?php echo base_url(); ?>assets/assets/js/Section/Menubar.js"></script>
    <script src="<?php echo base_url(); ?>assets/assets/js/Section/Sidebar.js"></script>
    <script src="<?php echo base_url(); ?>assets/assets/js/Section/PageAside.js"></script>
    <script src="<?php echo base_url(); ?>assets/assets/js/Plugin/menu.js"></script>

    <!-- Config -->
    <script src="<?php echo base_url(); ?>assets/global/js/config/colors.js"></script>
    <script src="<?php echo base_url(); ?>assets/assets/js/config/tour.js"></script>
    <script>Config.set('assets', '<?php echo base_url(); ?>assets/assets');</script>

    <!-- Page -->
    <script src="<?php echo base_url(); ?>assets/assets/js/Site.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/js/Plugin/asscrollable.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/js/Plugin/slidepanel.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/js/Plugin/switchery.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/js/Plugin/matchheight.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/js/Plugin/aspieprogress.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/js/Plugin/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/js/Plugin/asscrollable.js"></script>

    <script src="<?php echo base_url(); ?>assets/assets/examples/js/dashboard/team.js"></script>
    <script>var base_url = '<?php echo base_url() ?>';</script>

    <script src="<?php echo base_url(); ?>assets/global/vendor/datatables.net/jquery.dataTables.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/vendor/datatables.net-bs4/dataTables.bootstrap4.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/vendor/datatables.net-fixedheader/dataTables.fixedHeader.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/vendor/datatables.net-fixedcolumns/dataTables.fixedColumns.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/vendor/datatables.net-rowgroup/dataTables.rowGroup.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/vendor/datatables.net-scroller/dataTables.scroller.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/vendor/datatables.net-responsive/dataTables.responsive.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/vendor/datatables.net-responsive-bs4/responsive.bootstrap4.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/vendor/datatables.net-buttons/dataTables.buttons.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/vendor/datatables.net-buttons/buttons.html5.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/vendor/datatables.net-buttons/buttons.flash.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/vendor/datatables.net-buttons/buttons.print.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/vendor/datatables.net-buttons/buttons.colVis.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/vendor/datatables.net-buttons-bs4/buttons.bootstrap4.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/vendor/asrange/jquery-asRange.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/vendor/bootbox/bootbox.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/js/Plugin/datatables.js"></script>
    
    <script src="<?php echo base_url(); ?>assets/assets/examples/js/tables/datatable.js"></script>
    <script src="<?php echo base_url('assets/plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js')?>"></script>
    <script src="<?php echo base_url('assets/plugins/bower_components/sweetalert/sweetalert.min.js') ?>"></script>
    <script src="<?php echo base_url();?>assets/plugins/bower_components/toast-master/js/jquery.toast.js"></script>
    <script src="<?php echo base_url();?>assets/js/toastr.js"></script>
    <script src="<?php echo base_url('js/header.js') ?>"></script>
    

  </body>
  </html>