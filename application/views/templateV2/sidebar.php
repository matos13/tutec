   <div class="site-menubar site-menubar-light">
    <div class="site-menubar-body">
      <div>
        <div>
          <ul class="site-menu" data-plugin="menu">

            <li class="site-menu-category">Dashboard</li>
            <!-- --------------- -->
            <li class="dropdown site-menu-item has-sub">
              <a data-toggle="dropdown" href="<?php echo base_url();?>" data-dropdown-toggle="false">
                <i class="site-menu-icon wb-grid-4" aria-hidden="true"></i>
                <span class="site-menu-title">Dashboard</span>
                 
              </a>
 
            </li>


            <li class="site-menu-category">Antrian</li>
            <li class="dropdown site-menu-item has-sub">
              <a data-toggle="dropdown" href="<?php echo base_url();?>masterdata/counter" data-dropdown-toggle="false">
                <i class="site-menu-icon wb-grid-4" aria-hidden="true"></i>
                <span class="site-menu-title">Antrian</span>
                <!-- <span class="site-menu-arrow"></span> -->
              </a>
           
            </li>

             <li class="site-menu-category">List</li>
            <li class="dropdown site-menu-item has-sub">
              <a data-toggle="dropdown" href="<?php echo base_url();?>machine" data-dropdown-toggle="false">
                <i class="site-menu-icon wb-grid-4" aria-hidden="true"></i>
                <span class="site-menu-title">List</span>
                <!-- <span class="site-menu-arrow"></span> -->
              </a>
           
            </li>


            <!-- --------------- -->
            <li class="dropdown site-menu-item has-sub">
              <a data-toggle="dropdown" href="javascript:void(0)" data-dropdown-toggle="false">
                <i class="site-menu-icon wb-grid-4" aria-hidden="true"></i>
                <span class="site-menu-title">Master Data</span>
                <span class="site-menu-arrow"></span>
              </a>
              <div class="dropdown-menu">
                <div class="site-menu-scroll-wrap is-list">
                  <div>
                    <div>
                      <ul class="site-menu-sub site-menu-normal-list">
                        <li class="site-menu-item">
                          <a class="animsition-link" href="<?php echo base_url();?>masterdata/user_page">
                            <span class="site-menu-title">User</span>
                          </a>
                        </li>
                        <li class="site-menu-item">
                          <a class="animsition-link" href="<?php echo base_url();?>masterdata/poli_page">
                            <span class="site-menu-title">Poli</span>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </li>


          </ul>      
        </div>
        </div>
      </div>
    </div>
    <div class="page">