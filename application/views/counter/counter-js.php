<script>
 $(document).ready(function(){

    let panjang=$('[name^=waktu]').length;
        // console.log(panjang);

        $.ajax({
            url:"<?php echo base_url();?>dashboard/waktu",
            type: 'POST',
            dataType: 'JSON',

            success:function (data) {
                var i;
                for (var i = 0; i<panjang; i++) {
                    $('#waktu-hari-ini-'+i+'').html(data.waktu);
                }
                
            }
        });



        window.setInterval(function(){
            for (let i = 0; i<panjang; i++) {
                var poliNomor = $('#poli-'+i+'').val();

                // set interval untuk nilai max ketika panggil biar tidak refresh
                    // window.setInterval(function(){
                        $.ajax({
                            url:"<?php echo base_url();?>antrian/get_antrian_now",
                            data: {
                                'id':poliNomor,
                            },
                            cache: false,
                            dataType:'json',
                            success: function(data){
                                var max = $('#max-'+i+'').val(data[0].nomor);
                                // console.log('nilai max ajax',data[0].nomor)
                            }
                        });
                    // },2000);
                    // end set interval 

                    var antrian = $('#nomor-'+i+'').val();
                    var max = $('#max-'+i+'').val();

                    if (max==antrian) {
                        $("#button-"+i).prop('disabled', true);
                    }
                }
            }, 1000);

    });

 function panggil(nomor,poli) {

    var id = $('#id-'+nomor+'').val();


    if (id==0) {

        swal({
            title: "Antrian",
            text: "Antrian telah habis",
            type: "warning",
            closeOnConfirm: false
        });

    } else {

          var no=$('#nomor-'+nomor+'').val();
          var str=$('#nama-poli-'+nomor+'').val();
          var poli =str.toLowerCase()
          call(no,poli);
          
        // $.ajax({
        //     url:"<?php echo base_url();?>antrian/panggil",
        //     type: 'POST',
        //     data :{
        //         'id':id
        //     },
        //     dataType: 'JSON',

        //     success:function (data) {
        //         var no=$('#nomor-'+nomor+'').val();
        //         var str=$('#nama-poli-'+nomor+'').val();
        //         var poli =str.toLowerCase()
        //         call(no,poli);
        //     }
        // });
    }
}

function next(nomor) {
    var no=$('#nomor-'+nomor+'').val();
    var id = $('#id-'+nomor+'').val();
    var poliNomor = $('#poli-'+nomor+'').val();

    if (id==0) {
        swal({
            title: "Antrian",
            text: "Antrian telah habis",
            type: "warning",
            closeOnConfirm: false
        });

    } else {
        $.ajax({
            url:"<?php echo base_url();?>antrian/panggil",
            type: 'POST',
            data :{
                'id':id
            },
            dataType: 'JSON',

            success:function (data) {

                selanjutnya= parseInt(no)+1;
                var str=$('#nama-poli-'+nomor+'').val();
                var poli =str.toLowerCase();
                // console.log(poli);
                var kode;
                if (poli=='umum') {
                    kode ='A';
                }else if (poli=='gigi'){
                    kode ='B';
                }else if (poli=='pemeriksaan kehamilan'){
                    kode ='C';
                }else if (poli=='pengambilan obat'){
                    kode ='D';
                }

                $.ajax({
                    url:"<?php echo base_url();?>antrian/get_antrian_nexts",
                    data: {
                        'id':poliNomor,
                    },
                    cache: false,
                    dataType:'json',
                    success: function(data){
                            // console.log(data);
                            $('#id-'+nomor+'').val(data[0].minNomor);
                        }
                    });

        // window.setInterval(function(){

            var max = $('#max-'+nomor+'').val();
                    // console.log('nilai max otomatis sesuai klik',max)
                    document.getElementById('nomor_antrian-'+nomor+'').innerHTML = kode+''+selanjutnya+'/'+kode+''+max;
        // },3000);

                // $('#nomor_antrian-'+nomor+'').html(kode+''+selanjutnya+'/'+kode+''+max);
                $('#nomor-'+nomor+'').val(selanjutnya);
                var str=$('#nama-poli-'+nomor+'').val();
                var poli =str.toLowerCase();
                noString = selanjutnya.toString();
                call(noString,poli);
            }
        });

    }
}
function call(antrian,poli){
    // suara('bel',1000);
    console.log('call poli',poli);
    console.log('call antrian',antrian);
    
    document.getElementById('bel').pause();
    document.getElementById('bel').currentTime=0;
    document.getElementById('bel').play();
    setTimeout(function() {
        document.getElementById('bel').pause();
    },2500);
    //SET DELAY UNTUK MEMAINKAN REKAMAN NOMOR URUT
    // var waktu=document.getElementById('bel').duration*100;

    var waktu = 856;
    //MAINKAN SUARA NOMOR URUT
    // console.log(antrian);
    // waktu = suara(jenissuara,waktu-2500);
    waktu = suara('nomorurut',waktu+2000);
    if (poli=='umum') {
        waktu = suara('A',waktu+400);
    }else if (poli=='gigi'){
        waktu = suara('B',waktu+400);
    }else if (poli=='pemeriksaan kehamilan'){
        waktu = suara('C',waktu+400);
    }else if (poli=='pengambilan obat'){
        waktu = suara('D',waktu+400);
    }
    if( parseInt(antrian) < 12 ){

        waktu = suara(antrian,waktu+500);
        waktu = suara('loket',waktu+600);

    } else if(parseInt(antrian)<20){
        waktu = suara(antrian.charAt(1),waktu);
        waktu = suara('belas',waktu);
        waktu = suara('loket',waktu+600);

    }else if(parseInt(antrian)<30){
        waktu = suara(antrian.charAt(0),waktu);
        waktu = suara('puluh',waktu);
        if(antrian.charAt(1)!=0){
            waktu = suara(antrian.charAt(1),waktu);
        }
        waktu = suara('loket',waktu+600);
    } else if(parseInt(antrian)<100){
        waktu = suara(antrian.charAt(0),waktu+1000);
        waktu = suara('puluh',waktu);
        if(antrian.charAt(1)!=0){
            waktu = suara(antrian.charAt(1),waktu);
        }
        waktu = suara('loket',waktu+600);
    } else if(parseInt(antrian) < 200){
        if(parseInt(antrian.substr(1))<12){
            waktu = suara('seratus',waktu+1000);
            waktu = suara(parseInt(antrian.substr(1)),waktu);
            waktu = suara('loket',waktu+600);
        } else if(parseInt(antrian.substr(1))<20) {
            waktu = suara('seratus',waktu+1000);
            waktu = suara(antrian.charAt(2),waktu);
            waktu = suara('belas',waktu);
            waktu = suara('loket',waktu+600);
        } else {
            waktu = suara('seratus',waktu+1000);
            waktu = suara(antrian.charAt(1),waktu);
            waktu = suara('puluh',waktu)
            if(antrian.charAt(2)!=0){
                waktu = suara(antrian.charAt(2),waktu);
            }
            waktu = suara('loket',waktu+600);
        }
    } else if(parseInt(antrian)<1000){

        if(parseInt(antrian.substr(1))<12){
            waktu = suara(antrian.charAt(0),waktu+1000);
            waktu = suara('ratus',waktu);
            waktu = suara(parseInt(antrian.substr(1)),waktu);
            waktu = suara('loket',waktu+600);
        } else if(parseInt(antrian.substr(1))<20) {
            waktu = suara(antrian.charAt(0),waktu+1000);
            waktu = suara('ratus',waktu);
            waktu = suara(antrian.charAt(2),waktu);
            waktu = suara('belas',waktu);
            waktu = suara('loket',waktu+600);
        } else {
            waktu = suara(antrian.charAt(0),waktu+1000);
            waktu = suara('ratus',waktu);
            waktu = suara(antrian.charAt(1),waktu);
            waktu = suara('puluh',waktu);
            if(antrian.charAt(2)!=0){
                waktu = suara(antrian.charAt(2),waktu);
            }
            waktu = suara('loket',waktu+600);

        }
    }
    if (poli=='umum') {
        waktu = suara('umum',waktu+700);
    }else if (poli=='gigi'){
        waktu = suara('gigi',waktu+700);
    }else if (poli=='pemeriksaan kehamilan'){
        waktu = suara('kehamilan',waktu+700);
    }else if (poli=='pengambilan obat'){
        waktu = suara('obat',waktu+700);
    }

}
function suara(angka, waktu){
    document.getElementById(angka).volume ='1';
    setTimeout(function() {
      document.getElementById(angka).pause();
      document.getElementById(angka).currentTime=0;
      document.getElementById(angka).play();
  }, waktu);
    waktu+=1000;
    return waktu;
}
</script>