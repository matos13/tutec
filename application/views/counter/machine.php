<?php 
$this->load->view("templateV2/header.php") ;
$this->load->view("templateV2/sidebar.php") ;
?>
<audio  id="1" preload="none" src="<?php echo base_url('/assets/audio/1.MP3')?>"  ></audio>
<audio  id="2" preload="none" src="<?php echo base_url('/assets/audio/2.MP3')?>"  ></audio>
<audio  id="3" preload="none" src="<?php echo base_url('/assets/audio/3.MP3')?>"  ></audio>
<audio  id="4" preload="none" src="<?php echo base_url('/assets/audio/4.MP3')?>"  ></audio>
<audio  id="5" preload="none" src="<?php echo base_url('/assets/audio/5.MP3')?>"  ></audio>
<audio  id="6" preload="none" src="<?php echo base_url('/assets/audio/6.MP3')?>"  ></audio>
<audio  id="7" preload="none" src="<?php echo base_url('/assets/audio/7.MP3')?>"  ></audio>
<audio  id="8" preload="none" src="<?php echo base_url('/assets/audio/8.MP3')?>"  ></audio>
<audio  id="9" preload="none" src="<?php echo base_url('/assets/audio/9.MP3')?>"  ></audio>
<audio  id="10" preload="none" src="<?php echo base_url('/assets/audio/sepuluh.MP3')?>"  ></audio>
<audio  id="11" preload="none" src="<?php echo base_url('/assets/audio/sebelas.MP3')?>"  ></audio>
<audio  id="bel" preload="none" src="<?php echo base_url('/assets/audio/Airport_Bell.mp3')?>"  ></audio>
<audio  id="in" preload="none" src="<?php echo base_url('/assets/audio/in.wav')?>"  ></audio>
<audio  id="out" preload="none" src="<?php echo base_url('/assets/audio/out.wav')?>"  ></audio>
<audio  id="puluh" preload="none" src="<?php echo base_url('/assets/audio/puluh.MP3')?>"  ></audio>
<audio  id="ratus" preload="none" src="<?php echo base_url('/assets/audio/ratus.MP3')?>"  ></audio>
<audio  id="belas" preload="none" src="<?php echo base_url('/assets/audio/belas.MP3')?>"  ></audio>
<audio  id="loket" preload="none" src="<?php echo base_url('/assets/audio/loket.MP3')?>"  ></audio>
<audio  id="nomorurut" preload="none" src="<?php echo base_url('/assets/audio/nomor-urut.MP3')?>"  ></audio>
<audio  id="konter" preload="none" src="<?php echo base_url('/assets/audio/konter.MP3')?>"  ></audio>

<div class="page-header">
  <h1 class="page-title">Counter</h1>
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">Home</a></li>
    <li class="breadcrumb-item"><a href="#">Counter</a></li>
  </ol>
  <div class="page-header-actions">

    <a href="<?php echo base_url();?>machine"><button type="button" class="btn btn-sm btn-icon btn-danger btn-outline btn-round"
      data-toggle="tooltip" data-original-title="Refresh">
      <i class="icon wb-refresh" aria-hidden="true"></i>
    </button></a>
  </div>
</div>


<div class="page-content container-fluid" id="reloadPage">
  <div class="row" data-plugin="matchHeight" data-by-row="true">
    <?php 
    $array = json_decode(json_encode($poli), True);
        // echo "<pre>";
        // print_r($array);
        // echo "</pre>";


    foreach ($array as $key=> $value) {
      if (isset($value['nomor'])) {
        $nomor=$value['nomor'];
                        # code...
      } else {
        $nomor = "Habis";
      }
                    // $total=$value['total'];
      $poli=$value['poli_id'];

      if (isset($value['antrian_id'])) {
                        # code...
        $id = $value['antrian_id'];
      } else {
        $id=0;
      }
      //kode poli
      $kode;
      if (strtolower($value['nama'])=='umum') {
      $kode='A';
      }elseif(strtolower($value['nama'])=='gigi'){
      $kode='B';
      }elseif(strtolower($value['nama'])=='pemeriksaan kehamilan'){
        $kode='C';
      }elseif(strtolower($value['nama'])=='pengambilan obat'){
        $kode='D';
      }
      ?>


      <div class="col-xl-4 col-lg-6">
        <div class="card card-shadow card-inverse white bg-facebook">
          <div class="card-block p-20 h-full">
            <h2 class="white mt-0">Antrian</h2>
            <h1 class="white mt-0 font-size-40" id="nomor_antrian-<?php echo $key;?>" value=""><?php echo $kode.''.$nomor?>/<?php echo $kode.''.$value['max']?></h1>
            <h3 class="white mt-0">Poli <?php echo $value['nama']; ?></h4>
              <small><h5 class="white" id="waktu-hari-ini-<?php echo $key;?>" name="waktu"></h5></small>
              <input type="hidden" id="nomor-<?php echo $key;?>" value="<?php echo $nomor?>">
              <input type="hidden" id="poli-<?php echo $key;?>" value="<?php echo $poli?>">
              <input type="hidden" id="id-<?php echo $key;?>" value="<?php echo $id?>">
              <input type="hidden" id="max-<?php echo $key;?>" value="<?php echo $value['max'];?>">           
              <input type="hidden" id="nama-poli-<?php echo $key;?>" value="<?php echo $value['nama'];?>">

          </div>
        </div>
      </div>
      <br>
    <?php } ?>
  </div>
</div>


<?php
$this->load->view("templateV2/footer.php");
include 'machine-js.php';
?>
<script type="text/javascript">
    window.setInterval(function(){
      $( "#reloadPage" ).load(window.location.href + " #reloadPage" );
    },5000);


</script>