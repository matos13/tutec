       <!-- ============================================================== -->
       <!-- Left Sidebar - style you can find in sidebar.scss  -->
       <!-- ============================================================== -->
       <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav">
            <div class="sidebar-head">
                <h3><span class="fa-fw open-close"><i class="ti-menu hidden-xs"></i><i class="ti-close visible-xs"></i></span> <span class="hide-menu">Navigation</span></h3> </div>
                <ul class="nav" id="side-menu">
                    <li class="user-pro">
                        <a href="javascript:void(0)" class="waves-effect"><img src="<?php echo base_url('assets/plugins/images/users/varun.jpg')?>"" alt="user-img" class="img-circle"> <span class="hide-menu"> Steve Gection<span class="fa arrow"></span></span>
                        </a>
                        <ul class="nav nav-second-level collapse" aria-expanded="false" style="height: 0px;">
                            <li><a href="javascript:void(0)"><i class="ti-user"></i> <span class="hide-menu">My Profile</span></a></li>
                            <li><a href="javascript:void(0)"><i class="ti-wallet"></i> <span class="hide-menu">My Balance</span></a></li>
                            <li><a href="javascript:void(0)"><i class="ti-email"></i> <span class="hide-menu">Inbox</span></a></li>
                            <li><a href="javascript:void(0)"><i class="ti-settings"></i> <span class="hide-menu">Account Setting</span></a></li>
                            <li><a href="javascript:void(0)"><i class="fa fa-power-off"></i> <span class="hide-menu">Logout</span></a></li>
                        </ul>
                    </li>

                    <li> <a href="<?php echo base_url();?>" class="waves-effect"><i class="mdi mdi-av-timer fa-fw" data-icon="v"></i> <span class="hide-menu"> Dashboard </a>
                    <li> <a href="<?php echo base_url();?>masterdata/counter" class="waves-effect"><i  class="mdi mdi-settings fa-fw"></i> <span class="hide-menu">Counter</span></a> </li>
                    </li>
                    
                    <li> <a href="#" class="waves-effect "><i class="mdi mdi-content-copy fa-fw"></i> <span class="hide-menu">Master Data<span class="fa arrow"></a>
                        <ul class="nav nav-second-level two-li">
                            <li><a href="<?php echo base_url();?>masterdata/user_page"><i class=" ti-user"></i> <span class="hide-menu">User</span></a></li>

                            <li><a href="<?php echo base_url();?>masterdata/poli_page"><i class="  ti-wheelchair"></i> <span class="hide-menu">Poli</span></a></li>
                            
                            <li><a href="<?php echo base_url();?>masterdata/jadwaldokter_page"><i class=" ti-calendar"></i> <span class="hide-menu">Jadwal Dokter</span></a></li>

                            <li><a href="<?php echo base_url();?>masterdata/dokter_page"><i class=" ti-calendar"></i> <span class="hide-menu">Dokter</span></a></li>
                        </ul>
                    </li>

                </ul>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Left Sidebar -->
        <!-- ============================================================== -->