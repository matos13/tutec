
<?php $this->load->view("templateV2/header.php") ?>
<?php $this->load->view("templateV2/sidebar.php") ?>
<div class="page-content container-fluid">
  <div class="row" data-plugin="matchHeight" data-by-row="true">

    <?php 
    $array = json_decode(json_encode($poli), True);
    foreach ($array as $key=> $value) {
                    // print_r($value);
      $nomor=$value['nomor']+1;
      $poli=$value['poli_id'];
      $kode;
      if (strtolower($value['nama'])=='umum') {
      $kode='A';
      }elseif(strtolower($value['nama'])=='gigi'){
      $kode='B';
      }elseif(strtolower($value['nama'])=='pemeriksaan kehamilan'){
        $kode='C';
      }elseif(strtolower($value['nama'])=='pengambilan obat'){
        $kode='D';
      }
      ?>
      <div class="col-xl-4 col-lg-6">
        <div class="card card-shadow card-inverse white bg-facebook">
          <div class="card-block p-20 h-full">
            <h3 class="white mt-0">Antrian Nomor</h3>
            <h1 id="nomor_antrian-<?php echo $key;?>" class="white mt-0 font-size-50" value=""><?php echo $kode.''.$nomor?></h1>
            <small>
              <h5 class="white" id="waktu-hari-ini-<?php echo $key;?>" name="waktu"></h5>
            </small>
            <input type="hidden" id="nomor-<?php echo $key;?>" value="<?php echo $nomor?>">
            <input type="hidden" id="poli-<?php echo $key;?>" value="<?php echo $poli?>">        
            <input type="hidden" id="nama-poli-<?php echo $key;?>" value="<?php echo $value['nama'];?>">
            <h3 class="white">Poli <?php echo $value['nama']; ?></h3>

            <div class="mt-30" align="center">
              <!-- <i class="icon bd-info font-size-26"></i> -->
              <ul class="list-inline mt-15">
                <h5 class=" white">Tekan tombol dibawah untuk proses antrian</h5>
                <button onclick="antrian(<?php echo $key?>)" data-toggle="tooltip" data-placement="top" title="Tekan tombol untuk antrian Poli <?php echo $value['nama']?>" class="btn btn-lg btn-warning btn-rounded waves-effect waves-light m-t-20" >Pasien <?php echo $value['nama']; ?></button>
              </ul>
            </div>

          </div>
        </div>
      </div>


    <?php }?>
  </div>
</div>
<?php $this->load->view("templateV2/footer.php");
include 'dashboard-js.php';
?>