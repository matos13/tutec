
<div id="jadwaldoktermodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"  aria-hidden="true">
  <div class="modal-dialog" role="document">
   <div class="modal-content">
    <div class="modal-header" style="" >
     <h5 class="modal-title" id="exampleModalLabel">INPUT DATA ESTIMASI</h5>
     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
   <form action="#" id="form">
    <div class="form-body">
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label class="control-label">Nama Dokter</label>
            <input type="hidden" name="jd_id">
            <select name="jd_dokter" class="form-control">
              <?php 
              $array = json_decode(json_encode($dokter), True);
                                            // print_r($array);
              foreach ($array as $key=> $value) {
                ?>
                <option class="form-control" value="<?php echo $value['dokter_id']?>"><?php echo $value['dokter_nama']?></option>
              <?php } ?>
            </select>
          </div>
        </div>

      </div>
      <!--/row-->
      <div class="row">
        <!-- <div class="col-md-6"> -->
          <!--/span-->
          <div class="col-md-6">
            <div class="form-group">
              <label class="control-label">Nama Poli</label>

              <select name="jd_poli" class="form-control">
                <?php 
                $array = json_decode(json_encode($poli), True);
                foreach ($array as $key=> $value) {
                  ?>
                  <option class="form-control" value="<?php echo $value['poli_id']?>">Poli <?php echo $value['nama']?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <!--/span-->
          <div class="form-group">

           <!-- <div class="row"> -->
            <div class="col-md-12">
              <label class="control-label">Jadwal</label>
              <div class="form-group row">
                <div class="col-md-3">
                  <select name="jd_hari_mulai" class="form-control">
                    <option value="1" >Senin</option>
                    <option value="2" >Selasa</option>
                    <option value="3" >Rabu</option>
                    <option value="4" >Kamis</option>
                    <option value="5" >Jumat</option>
                    <option value="6" >Sabtu</option>
                    <option value="7" >Minggu</option>
                  </select>
                </div>
                 <div class="col-md-4">
                    <div class="input-group clockpicker">
                      <input name="jd_jam_mulai" type="text" class="form-control" > <span class="input-group-addon"> <span class="glyphicon glyphicon-time"></span> </span>
                    </div>
                  </div>
                   <div class="col-md-4">
                    <div class="input-group clockpicker">
                      <input name="jd_jam_selesai" type="text" class="form-control" > <span class="input-group-addon"> <span class="glyphicon glyphicon-time"></span> </span>
                    </div>
                  </div>

              </div>
            </div>

            </div>
          </div>
          <div class="form-group">
            <label>Kehadiran</label>          &nbsp;      <input type="checkbox" name="jd_kehadiran" value="1" >

          </div>

          <div class="form-actions">
            <a type="submit" onclick="save_jadwaldokter()" class="btn btn-success"> <i class="fa fa-check"></i> Save</a>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          </div>
        </form>

      </div>
    </div>
  </div>   

<style>
.popover {z-index: 1151 !important;}
</style>    
  <script type="text/javascript">
    $(function() {

      $('#single-input').clockpicker({
            placement: 'bottom'
            , align: 'left'
            , autoclose: true
            , 'default': 'now'
        });
        $('.clockpicker').clockpicker({
            donetext: 'Done'
            , }).find('input').change(function () {
                console.log(this.value);
            });
            $('#check-minutes').click(function (e) {
            // Have to stop propagation here
            e.stopPropagation();
            input.clockpicker('show').clockpicker('toggleView', 'minutes');
        });
            if (/mobile/i.test(navigator.userAgent)) {
                $('input').prop('readOnly', true);
            }
        // Colorpicker
        $(".colorpicker").asColorPicker();
        $(".complex-colorpicker").asColorPicker({
            mode: 'complex'
        });
        $(".gradient-colorpicker").asColorPicker({
            mode: 'gradient'
        });
        // Date Picker
        jQuery('.mydatepicker, #datepicker').datepicker();
        jQuery('#datepicker-autoclose').datepicker({
            autoclose: true
            , todayHighlight: true
        });
        jQuery('#date-range').datepicker({
            toggleActive: true
        });
        jQuery('#datepicker-inline').datepicker({
            todayHighlight: true
        });
        // Daterange picker
        $('.input-daterange-datepicker').daterangepicker({
            buttonClasses: ['btn', 'btn-sm']
            , applyClass: 'btn-danger'
            , cancelClass: 'btn-inverse'
        });
        $('.input-daterange-timepicker').daterangepicker({
            timePicker: true
            , format: 'MM/DD/YYYY h:mm A'
            , timePickerIncrement: 30
            , timePicker12Hour: true
            , timePickerSeconds: false
            , buttonClasses: ['btn', 'btn-sm']
            , applyClass: 'btn-danger'
            , cancelClass: 'btn-inverse'
        });
        $('.input-limit-datepicker').daterangepicker({
            format: 'MM/DD/YYYY'
            , minDate: '06/01/2015'
            , maxDate: '06/30/2015'
            , buttonClasses: ['btn', 'btn-sm']
            , applyClass: 'btn-danger'
            , cancelClass: 'btn-inverse'
            , dateLimit: {
                days: 6
            }
        });

      });
    </script>