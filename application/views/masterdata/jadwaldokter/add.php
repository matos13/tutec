<?php $this->load->view("template/header.php") ?>
<?php $this->load->view("template/sidebar.php") ?>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Master Data</h4> </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 

                    <ol class="breadcrumb">
                        <li><a href="javascript:void(0)">Master Data</a></li>
                        <li><a href="javascript:void(0)">Jadwal Dokter</a></li>
                        <li class="active">Add Jadwal Dokter Data</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /row -->
            <div class="row">
                <div class="col-sm-12">
                    <!--.row-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-info">
                                <div class="panel-heading">Form Jadwal Dokter </div>
                                <div class="panel-wrapper collapse in" aria-expanded="true">
                                    <div class="panel-body">
                <form action="#" id="form">
                    <div class="form-body">
                        <h3 class="box-title">Form Jadwal Dokter </h3>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Nama Dokter</label>
                                <input type="hidden" name="jd_id">
                                 <select name="jd_dokter" class="form-control">
                                            <?php 
                                            $array = json_decode(json_encode($dokter), True);
                                            // print_r($array);
                                            foreach ($array as $key=> $value) {
                                                ?>
                                                <option class="form-control" value="<?php echo $value['dokter_id']?>"><?php echo $value['dokter_nama']?></option>
                                            <?php } ?>
                                        </select>
                                </div>
                                </div>

                            </div>
                            <!--/row-->
                        <div class="row">
                            <!-- <div class="col-md-6"> -->
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Nama Poli</label>

                                        <select name="jd_poli" class="form-control">
                                            <?php 
                                            $array = json_decode(json_encode($poli), True);
                                            foreach ($array as $key=> $value) {
                                                ?>
                                                <option class="form-control" value="<?php echo $value['poli_id']?>">Poli <?php echo $value['nama']?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                    <!--/span-->
                                    <div class="form-group">

                                       <!-- <div class="row"> -->
                                        <div class="col-md-12">
                                            <label class="control-label">Jadwal</label>
                                            <div class="form-group row">
                                                <div class="col-md-2">
                                                    <select name="jd_hari_mulai" class="form-control">
                                                        <option value="1" >Senin</option>
                                                        <option value="2" >Selasa</option>
                                                        <option value="3" >Rabu</option>
                                                        <option value="4" >Kamis</option>
                                                        <option value="5" >Jumat</option>
                                                        <option value="6" >Sabtu</option>
                                                        <option value="7" >Minggu</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="input-group clockpicker">
                                                        <input name="jd_jam_mulai" type="text" class="form-control" placeholder="Choose start time"> <span class="input-group-addon"> <span class="glyphicon glyphicon-time"></span> </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-1" align="center">
                                                    <label>s/d</label>
                                                </div>
                                
                                                  <div class="col-md-2">
                                                    <div class="input-group clockpicker">
                                                        <input name="jd_jam_selesai" type="text" class="form-control" placeholder="Choose end time"> <span class="input-group-addon"> <span class="glyphicon glyphicon-time"></span> </span>
                                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Kehadiran</label>          &nbsp;      <input type="checkbox" name="jd_kehadiran" value="1" >

                                    </div>

                                    <!-- </div> -->

                                    <div class="form-actions">
                                        <a type="submit" onclick="save_jadwaldokter()" class="btn btn-success"> <i class="fa fa-check"></i> Save</a>
                                        <button type="button" class="btn btn-default">Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
               <!--./row-->
            </div>
        </div>
    </div> 
</div>
<?php $this->load->view("template/footer.php");
include 'jadwaldokter-js.php';
 ?>