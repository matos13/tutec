<div class="modal fade" id="doktermodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"  aria-hidden="true">
    <div class="modal-dialog" role="document">
     <div class="modal-content">
      <div class="modal-header" style="" >
       <h5 class="modal-title" id="exampleModalLabel"></h5>
       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
     <form action="#" id="form">
     <div class="form-body">
                <input type="hidden" name="dokter_id" id="dokter_id" value="" class="form-control">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">Nama Dokter</label>
                        <input type="text" name="dokter_nama" id="firstName" class="form-control" placeholder="Maria Elisabeth Siahaan"> <span class="help-block"> Nama lengkap dokter tanpa gelar </span> </div>
                        <input type="hidden" name="id_dokter" id="id_dokter" class="form-control" > <span class="help-block"></span> </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">Jenis Kelamin</label>
                        <select name="dokter_kelamin" class="form-control" name="">
                            <option value="1">Laki-Laki</option>
                            <option value="2">Perempuan</option>
                        </select> <span class="help-block"> Select your gender </span> </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 ">
                    <div class="form-group">
                        <label>Alamat</label>
                        <textarea class="form-control" name="dokter_alamat" placeholder="Alamat"></textarea>
                        <span class="help-block"> Tulis alamat dengan lengkap </span>
                </div>
            </div>

        </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">SIP Dokter</label>
                        <input type="text" name="dokter_sip" id="firstName" class="form-control" placeholder="028/158/SIP-TU/II/2003"> </div>
                </div>
                
            </div>
            <!--/row-->
                <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">No Identitas Dokter</label>
                        <input type="text" name="dokter_no_identitas" id="firstName" class="form-control" placeholder="KTP/SIM"> </div>
                </div>
            </div> 
  </form>
  <div class="modal-footer">
   <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
   <button  onclick="save_dokter()" class="btn btn-primary">Simpan</button>
   
 </div>
</div>
</div>
</div>   