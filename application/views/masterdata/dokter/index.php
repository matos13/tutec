<?php $this->load->view("template/header.php") ?>
<?php $this->load->view("template/sidebar.php") ?>

   <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Master Data</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                        
                        <ol class="breadcrumb">
                            <li><a href="javascript:void(0)">Master Data</a></li>
                            <li><a href="javascript:void(0)">Dokter</a></li>
                            <li class="active">Dokter Data Table</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0">Dokter Data Table <a href="<?php echo base_url();?>masterdata/dokter_add"> <button data-toggle="tooltip" data-placement="top" title="Add Data" class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"> <i class="ti-plus text-white"></i></button></a>

                            </h3>
                            <p class="text-muted m-b-30">Dokter data table</p>

                            <div class="table-responsive">
                                <table id="tabel-dokter" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Alamat</th>
                                            <th>Nomor Identitas</th>
                                            <th>Jenis Kelamin</th>
                                            <th>Nomor SIP</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        
<?php 
$this->load->view("template/footer.php");
include 'dokter-js.php';
include 'dokter-modal.php';
?>

