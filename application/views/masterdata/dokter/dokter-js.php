<script type="text/javascript">
    var save_method;
    var dokter;
    dokter = $('#tabel-dokter').DataTable({

    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "order": [], //Initial no order.

    // Load data for the table's content from an Ajax source
    "ajax": {
        "url": base_url+"dokter/ajax_list",
        "type": "POST"
    },

    //Set column definition initialisation properties.
    "columnDefs": [
    {
        "targets": [ -1 ], //last column
        "orderable": false, //set not orderable
    },
    ],

});
function reload_table_dokter()
{
    dokter.ajax.reload(null,false); //reload datatable ajax
}

  function save_dokter()
  {   
    var url;

    if(save_method == 'update') {
      url = base_url+"dokter/ajax_update";
    } else {
      url = base_url+"dokter/ajax_add";
    }

    // ajax adding data to database
    $.ajax({
      url : url,
      type: "POST",
      data: $('#form').serialize(),
      dataType: "JSON",
      success: function(data)
      {

        $.toast({
          heading: 'Data Berhasil Disimpan',
          position: 'top-right',
          loaderBg:'#ff6849',
          icon: 'success',
          hideAfter: 1500, 
          stack: 6
        });
        $('#doktermodal').modal('hide');
        reload_table_dokter();
        document.getElementById("form").reset();

        
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        $.toast({
          heading: 'Data gagal disimpan',
          position: 'top-right',
          loaderBg:'#ff6849',
          icon: 'error',
          hideAfter: 1500

        });
        document.getElementById("form").reset();
        

      }
    });
  }
function delete_dokter(id)
{

    swal({
        title: "Delete",
        text: "Apakah anda yakin akan menghapus data ?",
        type: "warning",
        showCancelButton: true,
        // confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        closeOnConfirm: false
    },
    function(){
        $.ajax({
        	url : base_url+"dokter/ajax_delete/"+id,
        	type: "POST",
        	dataType: "JSON",
            success:function(){
              swal('Data Berhasil Dihapus', ' ', 'success');
              $("#delete").fadeTo("slow", 0.7, function(){
                $(this).remove();
            })
              reload_table_dokter();

          },

      });
        
    });
}
function edit_dokter(id)
{
	save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
	  $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $.ajax({
        url : base_url+"dokter/ajax_edit/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            console.log(data);
            $('[name="id_dokter"]').val(data.dokter_id);
            $('[name="dokter_kelamin"]').val(data.dokter_kelamin);
            $('[name="dokter_nama"]').val(data.dokter_nama);
            $('[name="dokter_alamat"]').val(data.dokter_alamat);
            $('[name="dokter_sip"]').val(data.dokter_sip);
            $('[name="dokter_no_identitas"]').val(data.dokter_no_identitas);
            $('#doktermodal').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Data Dokter'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}
</script>

