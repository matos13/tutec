<?php $this->load->view("template/header.php") ?>
<?php $this->load->view("template/sidebar.php") ?>
 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Master Data</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                        
                        <ol class="breadcrumb">
                            <li><a href="javascript:void(0)">Master Data</a></li>
                            <li><a href="javascript:void(0)">Dokter</a></li>
                            <li class="active">Add Dokter Data</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /row -->
                <div class="row">
                    <div class="col-sm-12">
                              <!--.row-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading"> Dokter</div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">
                                    <form action="#" id="form">
                                        <div class="form-body">
                                            <h3 class="box-title">Data Dokter</h3>
                                            <hr>
                                             <input type="hidden" name="dokter_id" id="dokter_id" value="" class="form-control">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Nama Dokter</label>
                                                        <input type="text" name="dokter_nama" id="firstName" class="form-control" placeholder="Maria Elisabeth Siahaan"> <span class="help-block"> Nama lengkap dokter dengan gelar </span> </div>
                                                </div>
                                                
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Jenis Kelamin</label>
                                                        <select name="dokter_kelamin" class="form-control" name="">
                                                            <option value="1">Laki-Laki</option>
                                                            <option value="2">Perempuan</option>
                                                        </select> <span class="help-block"> Select your gender </span> </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6 ">
                                                    <div class="form-group">
                                                        <label>Alamat</label>
                                                        <!-- <input type="text" class="form-control"> </div> -->
                                                        <textarea class="form-control" name="dokter_alamat" placeholder="Alamat"></textarea>
                                                        <span class="help-block"> Tulis alamat dengan lengkap </span>
                                                </div>
                                            </div>
                                     
                                        </div>
                                         <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">SIP Dokter</label>
                                                        <input type="text" name="dokter_sip" id="firstName" class="form-control" placeholder="028/158/SIP-TU/II/2003"> </div>
                                                </div>
                                                
                                            </div>
                                            <!--/row-->
                                              <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">No Identitas Dokter</label>
                                                        <input type="text" name="dokter_no_identitas" id="firstName" class="form-control" placeholder="KTP/SIM"> </div>
                                                </div>
                                                
                                            </div>
                                            <!--/row-->
                                        <div class="form-actions">
                                            <a  class="btn btn-success" onclick="save_dokter()"> <i class="fa fa-check"></i> Save</a>
                                                <button type="button"  class="btn btn-default">Cancel</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--./row-->
                    </div>
                </div>
            </div>
        </div>

<?php $this->load->view("template/footer.php");
include 'dokter-js.php';
 ?>