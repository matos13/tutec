<?php $this->load->view("templateV2/header.php") ?>
<?php $this->load->view("templateV2/sidebar.php") ?>
<div id="page">

  <div class="page-header">
    <h1 class="page-title">Add Data User</h1>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item"><a href="#">Master Data</a></li>
      <li class="breadcrumb-item active"><a href="javascript:void(0)">Add Data User</a></li>
  </ol>
  <div class="page-header-actions">
      <button type="button" class="btn btn-sm btn-icon btn-default btn-outline btn-round"
      data-toggle="tooltip" data-original-title="Edit">
      <i class="icon wb-pencil" aria-hidden="true"></i>
  </button>
  <button type="button" class="btn btn-sm btn-icon btn-default btn-outline btn-round"
  data-toggle="tooltip" data-original-title="Refresh">
  <i class="icon wb-refresh" aria-hidden="true"></i>
</button>
<button type="button" class="btn btn-sm btn-icon btn-default btn-outline btn-round"
data-toggle="tooltip" data-original-title="Setting">
<i class="icon wb-settings" aria-hidden="true"></i>
</button>
</div>
</div>

<div class="page-content">
    <div class="panel">
      <div class="panel-body container-fluid">
        <div class="row row-lg">
          <div class="col-md-6">
            <!-- Example Basic Form (Form grid) -->
            <div class="example-wrap">
              <h4 class="example-title">Form Add User</h4>
              <div class="example">
                <form autocomplete="off" id="form">

                    <div class="form-group">
                        <label class="form-control-label" for="inputBasicEmail">User Name</label>
                        <input type="text" class="form-control" id="inputBasicEmail" name="user_username"
                        placeholder="Email Address" autocomplete="off" />
                    </div>
                    <div class="form-group">
                        <label class="form-control-label" for="inputBasicPassword">Password</label>
                        <input type="password" class="form-control" id="inputBasicPassword" name="user_password"
                        placeholder="Password" autocomplete="off" />
                    </div>
                    <div class="form-group">
                        <label class="form-control-label" for="inputBasicPassword">User Additional Information</label>
                        <textarea class="form-control" name="user_info" placeholder="Input User Info"></textarea>
                    </div>

                    <div class="form-actions">
                        <a  class="btn btn-success" onclick="save_user()"> <i class="fa fa-check"></i> Save</a>
                        <button type="button" class="btn btn-default">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- End Example Basic Form (Form grid) -->
    </div>

</div>
</div>
</div>
</div>


<?php $this->load->view("templateV2/footer.php");
include 'user-js.php';

?>