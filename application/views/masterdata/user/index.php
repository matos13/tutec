<?php $this->load->view("templateV2/header.php") ?>
<?php $this->load->view("templateV2/sidebar.php") ?>


<div class="page-header">
  <h1 class="page-title">Master Data User</h1>
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">Home</a></li>
    <li class="breadcrumb-item"><a href="#">Master Data</a></li>
    <li class="breadcrumb-item active"><a href="javascript:void(0)">Master Data User</a></li>
  </ol>
  <div class="page-header-actions">
    <a href="<?php echo base_url();?>masterdata/user_add"><button type="button" class="btn btn-sm btn-icon btn-success btn-outline btn-round"
      data-toggle="tooltip" data-original-title="Add Data">
      <i class="icon wb-plus" aria-hidden="true"></i>
    </button></a>
    <a href="<?php echo base_url();?>masterdata/user_page"><button type="button" class="btn btn-sm btn-icon btn-info btn-outline btn-round"
      data-toggle="tooltip" data-original-title="Refresh">
      <i class="icon wb-refresh" aria-hidden="true"></i>
    </button></a>
    
  </div>
</div>
<div class="page-content">
  <!-- Panel Basic -->
  <div class="panel">
    <header class="panel-heading">
      <div class="panel-actions"></div>
      <h3 class="panel-title">Master Data User</h3>
    </header>
    <div class="panel-body">
      <table id="tabel-user" class="table table-hover dataTable table-striped w-full" >
        
       <thead>
        <tr>
          <th>NO</th>
          <th>NAMA</th>
          <th>INFO</th>
          <th>AKSI</th>
        </tr>
      </thead>
    </table>
  </div>
</div>


</div>
<?php $this->load->view("templateV2/footer.php");
include 'user-js.php';
include 'user-modal.php';
?>

