 <!-- modal untuk edit -->
 <div class="modal fade" id="usermodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"  aria-hidden="true">
  <div class="modal-dialog" role="document">
   <div class="modal-content">
    <div class="modal-header" style="" >
     <h5 class="modal-title" id="exampleModalLabel">INPUT DATA ESTIMASI</h5>
     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
    <form id="form" autocomplete="off">
      <div class="form-group form-material" data-plugin="formMaterial">
        <label class="form-control-label" for="inputText">Username</label>
        <input type="text" name="user_username" id="firstName" class="form-control" placeholder="Username"
        />
      </div>
      <div class="form-group form-material" data-plugin="formMaterial">
        <label>User Additional Info</label>
        <textarea class="form-control" name="user_info" placeholder="User Additional Info"></textarea>
      </div>

      <div class="form-actions">
        <a  class="btn btn-success" onclick="save_user()"> <i class="fa fa-check"></i> Save</a>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
      </div>
    </form>

  </div>
</div> 