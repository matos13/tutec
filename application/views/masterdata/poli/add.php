<?php $this->load->view("templateV2/header.php") ?>
<?php $this->load->view("templateV2/sidebar.php") ?>
<div id="page">

  <div class="page-header">
    <h1 class="page-title">Add Data Poli</h1>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item"><a href="#">Master Data</a></li>
      <li class="breadcrumb-item active"><a href="javascript:void(0)">Add Data Poli</a></li>
  </ol>
  <div class="page-header-actions">
      <button type="button" class="btn btn-sm btn-icon btn-default btn-outline btn-round"
      data-toggle="tooltip" data-original-title="Edit">
      <i class="icon wb-pencil" aria-hidden="true"></i>
  </button>
  <button type="button" class="btn btn-sm btn-icon btn-default btn-outline btn-round"
  data-toggle="tooltip" data-original-title="Refresh">
  <i class="icon wb-refresh" aria-hidden="true"></i>
</button>
<button type="button" class="btn btn-sm btn-icon btn-default btn-outline btn-round"
data-toggle="tooltip" data-original-title="Setting">
<i class="icon wb-settings" aria-hidden="true"></i>
</button>
</div>
</div>

<div class="page-content">
    <div class="panel">
      <div class="panel-body container-fluid">
        <div class="row row-lg">
          <div class="col-md-6">
            <!-- Example Basic Form (Form grid) -->
            <div class="example-wrap">
              <h4 class="example-title">Form Add Poli</h4>
              <div class="example">
                <form autocomplete="off" id="form">


                    <div class="form-group">
                        <label class="form-control-label" for="inputBasicEmail">Nama Poli</label>
                        <input type="text" class="form-control" id="inputBasicEmail" name="nama"
                        placeholder="Nama Poli" autocomplete="off" />
                    </div>
                         <div class="form-group">
                        <label class="form-control-label">Status</label>
                        <div>
                          <div class="radio-custom radio-default radio-inline">
                            <input type="radio" id="inputBasicMale" name="status" checked="" value="1" />
                            <label for="inputBasicMale">Aktif</label>
                          </div>
                          <div class="radio-custom radio-default radio-inline">
                            <input type="radio" id="inputBasicFemale" name="status" value="0" />
                            <label for="inputBasicFemale">Tidak Aktif</label>
                          </div>
                        </div>
                      </div>

                    <div class="form-actions">
                        <a  class="btn btn-success" onclick="save_poli()"> <i class="fa fa-check"></i> Save</a>
                        <button type="button" class="btn btn-default">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- End Example Basic Form (Form grid) -->
    </div>

</div>
</div>
</div>
</div>


<?php $this->load->view("templateV2/footer.php");
include 'poli-js.php';

?>