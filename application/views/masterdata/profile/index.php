
<?php $this->load->view("templateV2/header.php") ?>
<?php $this->load->view("templateV2/sidebar.php") ?>
<?php 
$username = ($this->session->userdata['logged_in']['user_username']);
?>
<?php $user_info = json_decode(json_encode($user_info), True); ?>

<!-- Page -->
<div class="page">
  <div class="page-content container-fluid">
    <div class="row">
      <div class="col-lg-3">
        <!-- Page Widget -->
        <div class="card card-shadow text-center">
          <div class="card-block">

              <h4 class="profile-user"><?php echo $user_info['user_username']?></h4>
              <blockquote class="blockquote cover-quote font-size-16"><?php echo $user_info['user_info']?></blockquote>
             <!--  <p>Hi! I'm Adrian the Senior UI Designer at AmazingSurge. We hope
              you enjoy the design and quality of Social.</p> -->

          </div>

      </div>
      <!-- End Page Widget -->
  </div>

  <div class="col-lg-9">
    <!-- Panel -->
    <div class="panel">
      <div class="panel-body nav-tabs-animate nav-tabs-horizontal" data-plugin="tabs">
        <ul class="nav nav-tabs nav-tabs-line" role="tablist">
          <li class="nav-item" role="presentation"><a class="active nav-link" data-toggle="tab" href="#user_info"
              aria-controls="activities" role="tab">User Account Info</a></li>
              <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#settings" aria-controls="profile"
                  role="tab">Settings</a></li>
              </ul>

              <div class="tab-content">
                  <div class="tab-pane active animation-slide-left" id="user_info" role="tabpanel">
                    <ul class="list-group">

                    </ul>
                    <a class="btn btn-block btn-default profile-readMore" href="javascript:void(0)"
                    role="button">Show more</a>
                </div>

                <div class="tab-pane animation-slide-left" id="settings" role="tabpanel">
                    <ul class="list-group">

                      <li class="list-group-item">
                        <!-- <div class="media"> -->
                          <form id="form">
                            <div class="form-group">
                                <input type="hidden" name="user_id" value="<?php echo $user_info['user_id']; ?>">
                                <label class="col-md-12">Full Name</label>
                                <div class="col-md-12">
                                    <input type="text" placeholder="Full Name" value="<?php echo $user_info['user_username'];?>" name="user_username" class="form-control form-control-line"> </div>
                                </div>
                                <div class="form-group">
                                    <label for="example-email" class="col-md-12">User Info</label>
                                    <div class="col-md-12">
                                        <textarea placeholder="User Info" class="form-control form-control-line" name="user_info" id="example-email"><?php echo $user_info['user_info'];?></textarea> </div>
                                    </div>
                                <div class="form-group">

                                    <label for="example-email" class="col-md-12">Ganti password Anda disini</label>
                                    <label for="example-email" class="col-md-12">Masukan Password</label>
                                    <div class="col-md-12">
                                      <input type="password" placeholder="Enter Password" name="user_password" class="form-control form-control-line"> 
                                    </div>
                                    <label for="example-email" class="col-md-12">Masukan Lagi Password</label>

                                    <div class="col-md-12">
                                      <input type="password" placeholder="Retype Password" name="user_password_2" class="form-control form-control-line"> </div>
                                  </div>

                                    <div class="form-group">
                                        <div class="col-md-12">
                                        <a type="submit" onclick="update_profile()" class="btn btn-info"> <i class="fa fa-check"></i> Save</a>
                                        <button type="button" class="btn btn-danger">Cancel</button>
                                        </div>
                                    </div>
                                </form>
                                <!-- </div> -->
                            </li>
                        </ul>
                    </div>


                </div>
            </div>
        </div>
        <!-- End Panel -->
    </div>
</div>
</div>
</div>
<!-- End Page -->

<?php $this->load->view("templateV2/footer.php");
include 'profile-js.php';

?>